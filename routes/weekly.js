const express = require('express');
const redisClient = require('../services/redis');
const backendClient = require('../services/backend');
const router = express.Router();

function getDaysOfWeek(year, week) {
  const startOfYear = new Date(year, 0, 1);
  const weekOfYear = new Date(startOfYear);
  weekOfYear.setDate(startOfYear.getDate() + 7 * (week - 1));

  const range = Array.from({length: 7}, (_, i) => i);
  return range.map(i => {
    const dayOfWeek = new Date(weekOfYear);
    dayOfWeek.setDate(dayOfWeek.getDate() + i);
    return dayOfWeek;
  }).map(date => {
    // normalize offsets
    return new Date(date.getTime() - date.getTimezoneOffset() * 60 * 1000);
  });
}

async function getApodData(date) {
  const key = date.toISOString().split('T')[0];

  const cachedData = await redisClient.dataForDate(key);

  if (cachedData === null) {
    return await backendClient.dataForDate(key);
  } else {
    return cachedData;
  }
}

function buildReport(apodDays) {
  return apodDays.map(day => {
    return [day.date, day.title, day.mediaType]
  })
}

router.get('/:year/:week', async (req, res, next) => {
  const {year, week} = req.params;

  const days = getDaysOfWeek(year, week);

  try {
    const apodDays = await Promise.all(days.map(getApodData));
    const report = buildReport(apodDays);
    res.json(report);
  } catch (e) {
    next(e);
  }

});

module.exports = router;
