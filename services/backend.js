const axios = require('axios');

const fetchingHost = process.env.FETCHING_URL || 'http://localhost:3001';

module.exports = {
  async dataForDate(date) {
    const url = `${fetchingHost}/?date=${date}`;
    const response = await axios.get(url);
    return response.data;
  }
};
