const port = process.env.PORT || 3000;
const express = require('express');
const weeklyRoute = require('./routes/weekly');

const app = express();

app.use('/weekly', weeklyRoute);

app.listen(port, () => console.log(`'reporting' app listening on port ${port}`));
